<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref_no')->default(null);
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->longText('description');
            $table->integer('status')->comment('0-pending / 1- read / 2 - replied / 3 - resolved and closed');
            $table->unsignedBigInteger('replied_by')->default(null);
            $table->date('replied_at')->default(null);
            $table->timestamps();

            $table->foreign('replied_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
