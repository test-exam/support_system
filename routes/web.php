<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//site routes
Route::get('/', 'SiteController@index')->name('site.home');
Route::get('/ticket/create', 'TicketController@createTicket')->name('site.create-ticket');
Route::post('/ticket/save', 'TicketController@saveTicket')->name('site.save-ticket');
Route::get('/ticket/search', 'TicketController@searchTicket')->name('site.tickets.search');


//Admin routes
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/tickets', 'HomeController@supportTickets')->name('admin.tickets.view');
Route::post('/admin/tickets/status/done', 'HomeController@markDoneTicket')->name('admin.tickets.markDone');
Route::post('/admin/ticket/single', 'HomeController@singleTicket')->name('admin.ticket.single');
Route::post('/admin/ticket/reply', 'HomeController@replyTicket')->name('admin.ticket.reply');

