<?php


namespace App\Repository;


use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Support\Str;

class TicketRepository implements TicketRepositoryInterface
{

    public function saveTicketDetails($request)
    {

        $ticket = new Ticket();
        $ticket->ref_no = $this->generateReferenceNumber();
        $ticket->name = $request->name;
        $ticket->email = $request->email;
        $ticket->phone = $request->phone;
        $ticket->description = $request->description;
        $ticket->status = 0;
        $ticket->created_at = Carbon::now()->format('Y-m-d H:i:s');

        $res = $ticket->save();

        return $res;

    }

    public function searchTicket($ref){

        return Ticket::where('ref_no',$ref)->with('reply')->first();

    }

    private function generateReferenceNumber()
    {
        return Carbon::now()->format('is') . Str::random(12);
    }

}
