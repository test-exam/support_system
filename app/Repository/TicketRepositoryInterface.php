<?php


namespace App\Repository;


interface TicketRepositoryInterface
{
    public function saveTicketDetails($request);
    public function searchTicket($ref);
}
