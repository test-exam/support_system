<?php


namespace App\Repository;


use App\Models\Ticket;
use App\Models\TicketReply;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AdminRepository implements AdminRepositoryInterface
{
    public function getAllSupportTickets($query = null)
    {
        $tickets = Ticket::select('id', 'ref_no', 'name', 'email', 'phone', 'description', 'status', 'created_at', 'replied_by', 'replied_at')
            ->paginate(5);

        return $tickets;
    }

    public function changeTicketStatus($request)
    {
        return Ticket::where('id', $request->id)->update(['status' => $request->status]);
    }

    public function getSingleTicketDetails($request)
    {
        Ticket::where('id',$request->id)->update(['status'=>1]);
        return Ticket::where('id', $request->id)->first();
    }

    public function replyTicket($request)
    {
        $reply = new TicketReply();
        $reply->ticket_id = $request->id;
        $reply->reply = $request->reply;
        $reply->user_id = Auth::user()->id;
        $reply->status = 1;
        $reply->created_at = Carbon::now()->format('Y-m-d H:i:s');

        $res = $reply->save();

        if ($res){
            return Ticket::where('id',$request->id)->update(['status'=>2]);
        }

    }


}
