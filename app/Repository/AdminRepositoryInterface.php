<?php


namespace App\Repository;


interface AdminRepositoryInterface
{
    public function getAllSupportTickets($query = null);
    public function changeTicketStatus($request);
    public function getSingleTicketDetails($request);
    public function replyTicket($request);
}
