<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Repository\TicketRepositoryInterface;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    private $ticketRepository;

    //
    public function __construct(TicketRepositoryInterface $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function createTicket()
    {
        return view('pages.ticket-create');
    }

    public function saveTicket(Request $request)
    {

        if ($this->ticketRepository->saveTicketDetails($request)) {
//            $details = [
//                'email' => $request->email,
//                'name' => $request->name,
//                'phone' => $request->phone,
//                'description' => $request->description,
//                ];
//            SendEmail::dispatch($details);
            return redirect()->back()->with('success', 'Your ticket was submitted successfully! One of our team member will get back to you soon.');
        } else {
            return redirect()->back()->with('error', 'Your ticket was not submitted! Please try again.');
        }

    }

    public function searchTicket(Request $request){
        $details = $this->ticketRepository->searchTicket($request->search);
        if(!$details){
            abort(404);
        }
        return view('pages.ticket-details')->with('details',$details);
    }
}
