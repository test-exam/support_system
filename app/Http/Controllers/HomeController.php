<?php

namespace App\Http\Controllers;

use App\Repository\AdminRepositoryInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $adminRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        $this->adminRepository = $adminRepository;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function supportTickets()
    {
        $tickets = $this->adminRepository->getAllSupportTickets();
        return view('admin.tickets-all')
            ->with('tickets', $tickets);
    }

    public function markDoneTicket(Request $request)
    {
        $res = $this->adminRepository->changeTicketStatus($request);
        return response()->json(['status' => $res]);
    }

    public function singleTicket(Request $request)
    {
        $res = $this->adminRepository->getSingleTicketDetails($request);
        return response()->json($res);
    }

    public function replyTicket(Request $request)
    {
        $res = $this->adminRepository->replyTicket($request);
        return response()->json(['status' => $res]);
    }
}
