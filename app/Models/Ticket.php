<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function reply(){
        return $this->hasMany('App\Models\TicketReply');
    }
}
