@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="mb-3">View Support Tickets</h2>

                        <table class="table">
                            <thead>
                            <tr class="bg-light">
                                <th scope="col">#</th>
                                <th scope="col">Ref No</th>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Problem Description</th>
                                <th scope="col">Status</th>
                                <th scope="col">Submitted On</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $x=1; @endphp
                            @foreach($tickets as $ticket)
                                <tr {{($ticket->status == 0) ? 'class=highlight title=Pending' : ""}} >
                                    <th scope="row">{{$x}}</th>
                                    <td>{{$ticket->ref_no}}</td>
                                    <td>{{$ticket->name}}</td>
                                    <td>{{$ticket->email}}</td>
                                    <td>{{$ticket->phone}}</td>
                                    <td>{{\Illuminate\Support\Str::limit($ticket->description, 100)}}</td>
                                    <td>
                                        @if($ticket->status == 0)
                                            <span class="badge badge-danger">Pending</span>
                                        @elseif($ticket->status == 1)
                                            <span class="badge badge-warning">Read</span>
                                        @elseif($ticket->status == 2)
                                            <span class="badge badge-info">Replied</span>
                                        @else
                                            <span class="badge badge-success">Done</span>
                                        @endif
                                    </td>
                                    <td>{{\Carbon\Carbon::parse($ticket->created_at)->diffForHumans()}}</td>
                                    <td>
                                        @if($ticket->status == 0 OR $ticket->status == 1)
                                            <button class="btn btn-sm btn-info" onclick="viewTicket({{$ticket->id}})">View and Reply</button>
                                        @elseif($ticket->status == 2)
                                            <button class="btn btn-sm btn-success"
                                                    onclick="markAsDone({{$ticket->id}})">Mark As Done
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                                @php $x++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="p-3 text-center">
                    {{$tickets->links()}}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tiketDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title ">Support Ticket from <b class="c_name"></b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <b>Name : </b> <span class="c_name"></span> <br>
                    <b>Email : </b> <span class="email"></span> <br>
                    <b>Phone : </b> <span class="phone"></span> <br>
                    <b>submitted on : </b> <span class="date"></span> <br>
                    <b>Problem Description : </b> <span class="des"></span> <br>

                    <hr>
                    <h3 class="p-3">Send a reply</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Reply</label>
                                <input type="text" id="reply_id" name="id">
                                <textarea class="form-control" id="reply" rows="5" name="reply"></textarea>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success btn-sm m-3" onclick="sendReply()">Submit</button>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        var path = "{{route('admin.tickets.markDone')}}";
        var view_path = "{{route('admin.ticket.single')}}";
        var reply_path = "{{route('admin.ticket.reply')}}";

        function markAsDone(id) {
            if (confirm('Are you sure to mark this as done?')) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: path,
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id: id, status: 4},
                    dataType: 'JSON',
                    success: function (data) {
                       if (data.status){
                           location.reload();
                       }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        }

        function viewTicket(id){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: view_path,
                type: 'POST',
                data: {_token: CSRF_TOKEN, id: id},
                dataType: 'JSON',
                success: function (data) {
                    if (data){
                        console.log(data.name)
                        $('#tiketDetailsModal .c_name').html(data.name);
                        $('#tiketDetailsModal #reply_id').val(data.id);
                        $('#tiketDetailsModal .email').html(data.email);
                        $('#tiketDetailsModal .phone').html(data.phone);
                        $('#tiketDetailsModal .date').html(data.created_at);
                        $('#tiketDetailsModal .des').html(data.description);
                        $("#tiketDetailsModal").modal()
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }

        function sendReply(){
            var reply = $('#reply').val();
            var id = $('#reply_id').val();
            if (reply == ''){
                $('#reply').css('border','red');
            }else{
                $('#reply').css('border','#ced4da');
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: reply_path,
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, reply: reply, id:id},
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.status){
                            location.reload();
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }

        }

    </script>
@endsection
