@extends('layouts.layout')

@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12 p-5 bg-light text-center">
                <h2 class="text-center">Welcome to Online Support System</h2>
                <hr>
                <a href="{{route('site.create-ticket')}}" class="btn btn-info btn-lg">Create a Ticket</a>
                <hr>
                <p>OR</p>
                <hr>
                <h4 class="m-4">Search a ticket here</h4>
                <form class="form-inline align-items-center mt-2" style="display: block !important;" action="{{route('site.tickets.search')}}">
                    <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Ref No" name="search" required>

                    <button type="submit" class="btn btn-primary mb-2">Search</button>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('extra-js')

@endsection

@section('extra-css')
    <style>
        .error {
            color: red;
        }
    </style>
@endsection
