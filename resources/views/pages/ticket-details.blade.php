@extends('layouts.layout')

@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12 p-5 bg-light">
                <h2 class="">Support Ticket details for <u>{{$details->ref_no}}</u></h2>
                <hr>
                <ul class="list-group">
                    <li class="list-group-item">Name: {{$details->name}}</li>
                    <li class="list-group-item">Email: {{$details->email}}</li>
                    <li class="list-group-item">Phone: {{$details->phone}}</li>
                    <li class="list-group-item">description: {{$details->description}}</li>
                    <li class="list-group-item">Status:
                        @if($details->status == 0)
                            <span class="badge badge-danger">Pending</span>
                        @elseif($details->status == 1)
                            <span class="badge badge-warning">Read</span>
                        @elseif($details->status == 2)
                            <span class="badge badge-info">Replied</span><br>
                        @else
                            <span class="badge badge-success">Done</span>
                        @endif
                    </li>
                </ul>

                <hr>
                @if(count($details->reply))
                    <h3>Replies</h3>
                    <ul class="list-group">
                        @foreach($details->reply as $reply)
                            <li class="list-group-item">{{$reply->reply}} <br>
                            <small class="text-muted">On {{\Carbon\Carbon::parse($reply->created_at)->diffForHumans()}}</small>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('extra-js')

@endsection

@section('extra-css')
    <style>
        .error {
            color: red;
        }
    </style>
@endsection
